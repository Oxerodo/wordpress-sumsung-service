<?php
if (post_password_required()) {
	return;
}
$sunsungservece_one_comment_count = get_comments_number(); ?>
<?php if (have_comments()) : ?>
	<?php wp_list_comments([
		'style' => 'div',
		'type' => 'comment',
		'per_page' => 3,
		'callback' => 'themeCommentsViewList',
	]); ?>
	<div class="pagination d-flex justify-content-center">
		<?php $pagination = themeCommentsPaginationGetArray( paginate_comments_links(['type'=>'array','show_all'=>true]) );
		$max = get_comment_pages_count();
		?>
		<a class="pagination-link" href="<?=get_comments_pagenum_link(1,$max)?>">
			<div class="pagination-button pagination-first-button">Первая</div>
		</a>
		<?php foreach ($pagination as $page): ?>
			<?php if (isset($page['pos']) && ($page['pos'] == 'next' || $page['pos'] == 'prev')): ?>
				<a class="pagination-link" href="<?=$page['val'][1]?>">
					<div class="pagination-button pagination-<?=$page['pos']?>-button"></div>
				</a>
			<?php elseif(isset($page['pos']) && $page['pos'] == 'current'):?>
				<div class="pagination-button pagination-page-button active"><?=$page['text']?></div>
			<?php else: ?>
				<a class="pagination-link" href="<?=$page['val'][1]?>">
					<div class="pagination-button pagination-page-button"><?=$page['text']?></div>
				</a>
			<?php endif; ?>
		<?php endforeach; ?>
		<a class="pagination-link" href="<?=get_comments_pagenum_link($max)?>">
			<div class="pagination-button pagination-last-button">Последняя</div>
		</a>
	</div>
<?php endif; ?>
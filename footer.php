<footer>
    <div class="container">
        <div class="row">
            <div class="footer-logotype-section col-1-5 md-col-1-1 md-d-flex md-justify-content-between">
                <div class="footer-logotype-wrapper">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/logotype-footer.svg" alt="" class="image-footer-logotype">
                    <span class="footer-logotype-subtitle">Официальный сервисный центр</span>
                </div>
                <div class="footer-payment-wrapper md-d-none">
                    <div class="footer-payment-title">Принимаем к оплате:</div>
                    <div class="footer-payment-images-wrapper d-flex align-items-center">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/image-payment-visa.svg" alt="visa" class="image-payment-footer image-footer-payment">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/image-payment-mastercard.svg" alt="mastercard" class="image-payment-footer image-footer-payment">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/image-payment-mir.svg" alt="mir" class="image-payment-footer image-footer-payment">
                    </div>
                </div>
                <div class="footer-contacts-wrapper d-none md-d-block">
                    <a class="footer-phone" href="tel:88009999999">8 (800) 999-99-99</a>
                    <p class="footer-phone-subtitle">Бесплатный звонок по РФ</p>
                    <a class="footer-phone" href="tel:88009999999">7 (999) 999-99-99</a>
                    <p class="footer-phone-subtitle">Ежедневно с 08:00 до 22:00</p>
                </div>
            </div>
            <div class="footer-first-menu col-1-5 col-md-30 sm-col-1-1">
                <?php viewHeadMenu([
                    'theme_location' => 'foot_menu_01',
                    'container' => false,
                    'menu_class' => 'page-menu d-flex flex-wrap justify-content-between md-d-block',
                    'menu_item_class' => 'item-page-menu',
                    'menu_item_link_class' => 'item-page-menu-link'
                ]); ?>
            </div>
            <div class="footer-second-menu col-1-5 col-md-30 sm-col-1-1">
                <?php viewHeadMenu([
                    'theme_location' => 'foot_menu_02',
                    'container' => false,
                    'menu_class' => 'service-menu service-menu-1',
                    'menu_item_class' => 'item-service-menu',
                    'menu_item_link_class' => 'item-page-menu-link'
                ]); ?>
            </div>
            <div class="footer-third-menu col-1-5 col-md-40 sm-col-1-1">

                <?php viewHeadMenu([
                    'theme_location' => 'foot_menu_03',
                    'container' => false,
                    'menu_class' => 'service-menu service-menu-1',
                    'menu_item_class' => 'item-service-menu',
                    'menu_item_link_class' => 'item-page-menu-link'
                ]); ?>
            </div>
            <div class="col-1-5 md-d-none">
                <div class="footer-contacts-wrapper">
                    <a class="footer-phone" href="tel:88009999999">8 (800) 999-99-99</a>
                    <p class="footer-phone-subtitle">Бесплатный звонок по РФ</p>
                    <a class="footer-phone" href="tel:88009999999">7 (999) 999-99-99</a>
                    <p class="footer-phone-subtitle">Ежедневно с 08:00 до 22:00</p>
                    <a href="mailto:info@samsung-servicecenter.ru" class="footer-email">info@samsung-servicecenter.ru</a>
                    <p class="footer-timework-title">Режим работы:</p>
                    <p class="footer-timework">Пн-Вс: 09.00-20.00</p>

                </div>
            </div>
            <div class="md-col-1-1 d-none md-d-flex sm-flex-wrap">
                <div class="md-col-footer col-footer-1 md-pl-30">
                    <a href="mailto:info@samsung-servicecenter.ru" class="footer-email">info@samsung-servicecenter.ru</a>
                    <button class="btn btn-footer-callback">Заказать звонок</button>
                </div>
                <div class="md-col-footer col-footer-2 md-text-center">
                    <p class="footer-timework-title">Режим работы:</p>
                    <p class="footer-timework">Пн-Вс: 09.00-20.00</p>
                </div>
                <div class="md-col-footer col-footer-3 md-align-items-end sm-align-items-start">
                    <div class="footer-payment-title">Принимаем к оплате:</div>
                    <div class="footer-payment-images-wrapper d-flex align-items-center">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/image-payment-visa.svg" alt="visa" class="image-payment-footer image-footer-payment">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/image-payment-mastercard.svg" alt="mastercard" class="image-payment-footer image-footer-payment">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/image-payment-mir.svg" alt="mir" class="image-payment-footer image-footer-payment">
                    </div>
                </div>
            </div>
            <div class="footer-end-line d-flex justify-content-between sm-flex-wrap">
                <p class="copyright">© 2021 Официальный сервисный центр Samsung.</p>
                <p class="privacy-policy-wrapper">
                    <a href="#" class="privacy-polycy-link">Политика конфиденциальности</a>
                </p>
                <button class="btn btn-footer-callback md-d-none">Заказать звонок</button>
            </div>
        </div>
    </div>
</footer>
<?php get_template_part('template-parts/tpl/form','application'); ?>
<?php get_template_part('template-parts/tpl/form','checked'); ?>
<?php wp_footer(); ?>
</body>

</html>
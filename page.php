<?php get_header() ?>
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<section class="contacts">
			<div class="container">
				<div class="row">
					<div class="breadcrumbs wrapper">
						<?php do_action('add_breadcrumbs'); ?>
					</div>
				</div>

				<div class="row">
					<h1 class="title title-contacts"><?php echo viewTitle() ?></h1>
					<?php if (!empty($contact_info = get_field('contacts_info'))) : ?>
						<?php foreach ($contact_info as $row) : ?>
							<div class="col-45">
								<p class="title-contacts-row"><?php echo $row['title'] ?></p>
							</div>
							<div class="col-55">
								<p class="value-contacts-row"><?php echo $row['description'] ?></p>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<?php if ($map = get_field('yandex_map_code')) : ?>
					<div class="row">
						<div class="wrapper wrapper-map-contacts" id="contacts-map">
							<?php echo $map ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</section>
		<?php get_template_part('template-parts/content/content', null, ['class' => 'bg-gray']) ?>
		<section class="form_contacts">
			<div class="container-fluid">
				<div class="row">
					<div class="col-55 pl-0">
						<div class="form_contacts_image"></div>
					</div>
					<div class="col-45 pr-0 form_contacts_wrapper">
						<form action="#" class="form_contacts_inner">
							<h2 class="form_title">Обратная связь</h2>
							<p class="form-field-name">Ваше имя</p>
							<input type="text" class="input-form" name="name" value="" placeholder="Укажите ваше имя">
							<p class="form-field-name">Ваш телефон</p>
							<input type="text" class="input-form" name="name" value="" placeholder="+ 7 999 999 99 99">
							<p class="form-field-name">Ваш E-mail</p>
							<input type="text" class="input-form" name="name" value="" placeholder="Укажите ваш E-mail">
							<button class="btn btn-submit">Отправить</button>
						</form>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php else : ?>
	<?php get_template_part('template-parts/content/content', 'none'); ?>
<?php endif; ?>


<?php get_footer() ?>
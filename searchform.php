<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>" class="input-group mb-3">
    <input name="s" id="s" type="text" class="form-control input-search" placeholder="Поиск" value="<?php echo get_search_query() ?>">
    <button id="searchsubmit" class="btn btn-search-submit" type="submit"></button>
</form>
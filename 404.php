<?php get_header() ?>
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<section class="contacts">
			<div class="container">
				<div class="row">
					<div class="breadcrumbs wrapper">
						<?php do_action('add_breadcrumbs'); ?>
					</div>
				</div>

				<div class="row">
					<h1 class="title title-contacts">404 Страница</h1>
				</div>

			</div>
		</section>
		<?php // get_template_part('template-parts/content/content', null, ['class' => 'bg-gray']) ?>

	<?php endwhile; ?>
<?php else : ?>
	<?php get_template_part('template-parts/content/content', 'none'); ?>
<?php endif; ?>


<?php get_footer() ?>
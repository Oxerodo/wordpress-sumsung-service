<?php
include_once 'assets/class/breadcrumbs.php';
include_once 'assets/class/walker.php';
include_once 'assets/include/comments.php';

add_theme_support('title-tag');
add_theme_support('post-thumbnails');
if (function_exists('acf_add_options_page')) {
	acf_add_options_page();
}
add_theme_support( 'html5', array(
	'comment-list',
	'comment-form',
	'search-form',
	'gallery',
	'caption',
	'script',
	'style',
) );
add_action('wp_enqueue_scripts', 'theme_add_scripts');
function theme_add_scripts()
{
	wp_enqueue_style('style-app', get_template_directory_uri() . '/assets/css/app.min.css', array(), '1.0', false);
	wp_enqueue_style('style-custom', get_template_directory_uri() . '/assets/css/custom.css', array(), '1.0', false);
	wp_enqueue_script('script-app-min', get_template_directory_uri() . '/assets/js/app.min.js', array(), '1.0', true);
	wp_enqueue_script('script-reating', get_template_directory_uri() . '/assets/js/reating.js', array(), '1.0', true);
	wp_enqueue_script('script-app-js', get_template_directory_uri() . '/assets/js/app.js', array(), '1.0', true);
}

add_action('after_setup_theme', function () {
	register_nav_menus([
		'head_menu' => 'Верхнее меню',
		'nav_menu' => 'Основное меню',
		'foot_menu_01' => 'Нижнее меню 1',
		'foot_menu_02' => 'Нижнее меню 2',
		'foot_menu_03' => 'Нижнее меню 3',
	]);
});

add_action( 'after_setup_theme', function () {
	add_image_size( 'sumsungservice-mainbanner', 700, 700, false );
	add_image_size( 'sumsungservice-devicepreview', 210, 220, false );
});



add_action('add_breadcrumbs', 'get_breadcrumps');
function get_breadcrumps()
{
	if (function_exists('kama_breadcrumbs')) kama_breadcrumbs(' / ', ['home' => 'Samsung'], ['show_term_title' => true]);
}

add_filter( 'upload_mimes', 'svg_upload_allow' );

# Добавляет SVG в список разрешенных для загрузки файлов.
function svg_upload_allow( $mimes ) {
	$mimes['svg']  = 'image/svg+xml';

	return $mimes;
}

add_filter( 'the_content', 'getCityNameFromContent' );
function getCityNameFromContent( $content ) {
	// $content = str_replace('[acf&nbsp;field="city_name"]', get_field('city_name', 'option'), $content);
	$content = str_replace('[acf field="city_name"]', get_field('city_name', 'option'), $content);
	return $content;
}

/**
 * КАСТОМНЫЕ ФУНКЦИИ
 *
 */
/**
 * Вывести заголовок
 */
function viewTitle(){
	$value = str_replace('Samsung', '<span>Samsung</span>', get_the_title());
	$value = str_replace('[[!++city_name]]', get_field('city_name', 'option'), $value);
	return $value;
}

/**
 * Слово Sumsug обрамить тегом <span>
 * [acf field="city_name"] оборачиваем в тексте
 */
function getWrapperSumsung($value){
	$value = str_replace('[acf field="city_name" post_id="option"]', get_field('city_name', 'option'), $value);
	$value = str_replace('[acf field="city_name"]', get_field('city_name', 'option'), $value);
	$value = str_replace('Samsung', '<span>Samsung</span>', $value);
	return $value;
}
function getCityName($value){
	$value = str_replace('[acf field="city_name"]', get_field('city_name', 'option'), $value);
	return $value;
}
/**
 * Вывести меню 
 * viewHeadMenu ($args = array())
 */
function viewHeadMenu( $args ) {
	
	$args = array_merge( [
		'container'       => 'div',
		'container_id'    => 'top-navigation-primary',
		'container_class' => 'top-navigation',
		'menu_class'      => 'menu main-menu menu-depth-0 menu-even',
		'echo'            => false,
		'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth'           => 10,
		'walker'          => new Walker_Bulder_Head_Menu()
	], $args );
	
	echo wp_nav_menu( $args );
}


add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});



// Дружим ACF и Yaost SEO
// что возвращаем
function get_city_name() {
    $city = get_field('city_name', 'option');
	return $city;
    // здесь пишем код, который выводит поле из группы. Обязательно вывод должен быть через return 
}
 
// регистрируем свой сниппет (меняем city_name - на название сниппета, get_city_name - на название функции выше)
function register_custom_yoast_variables() {
    wpseo_register_var_replacement( '%%city_name%%', 'get_city_name', 'advanced', 'Город' );
}
 
// цепляем за wpseo_register_extra_replacements
add_action('wpseo_register_extra_replacements', 'register_custom_yoast_variables');

add_action( 'template_redirect', function() {
	if( is_category(7) ){
		wp_redirect( '/remont-noutbukov/', 301 );
		exit;
	}
} );


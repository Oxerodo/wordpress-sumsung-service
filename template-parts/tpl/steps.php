<section class="steps">
    <div class="container">
        <div class="row">
            <div class="wrapper">
                <?php if ($title = get_field('h2_title_steps', 'option')) : ?>
                    <h2 class="steps-title"><?php echo $title ?></h2>
                <?php endif; ?>
            </div>
        </div>
        <?php if ($case = get_field('case_steps', 'option')):?>
        <div class="row">
            <div class="wrapper steps-wrapper d-flex flex-wrap justify-content-between md-justify-content-around js-sm-slider-steps">
                <?php foreach ($case as $item):?>
                <div class="card-step">
                    <div class="card-step-image-wrapper">
                        <?php $img = wp_get_attachment_image_url($item['image-step-id'], 'full'); ?>
                        <?php $alt = get_post_meta($item['image-step-id'], '_wp_attachment_image_alt', true); ?>
                        <img src="<?php echo $img ?>" alt="<?php echo $alt ?>" class="card-step-image">
                    </div>
                    <div class="card-step-title"><?php echo $item['item-step-title']?></div>
                    <div class="card-step-description"><?php echo $item['item-step-description'] ?></div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php if ($property = get_field('list_property')):?>
<?php list($property_left, $property_right) = array_chunk($property, ceil(count($property)/2)); ?>
<section class="property">
    <div class="container">
        <div class="row">
            <h2 class="title title-property">Характеристики <?php the_field('type_vehicle')?> <?php the_field('model');?></h2>
            <?php if (!empty($property_left)):?>
            <div class="col-1-2">
                <div class="row">
                    <?php  foreach ($property_left as $row): if ($row['is_show']): ?>
                    <div class="col-1-2 proprty-title"><?php echo $row['title'] ?>:</div>
                    <div class="col-1-2 property-value"><?php echo $row['value'] ?></div>
                    <?php endif; endforeach; ?>
                    <?/*
                    <div class="col-1-2 proprty-title">Dual Bluetooth Headset:</div>
                    <div class="col-1-2 property-value">есть</div>
                    <div class="col-1-2 proprty-title">EMS:</div>
                    <div class="col-1-2 property-value">есть + до 10 на каждую <br>модель в сумме</div>
                    <div class="col-1-2 proprty-title">Вес:</div>
                    <div class="col-1-2 property-value">196 г</div>
                    <div class="col-1-2 proprty-title">Диагональ: </div>
                    <div class="col-1-2 property-value">5 дюйм.</div>
                    <div class="col-1-2 proprty-title">Дополнительный экран: </div>
                    <div class="col-1-2 property-value">есть, цветной, 64x80 пикс.</div>
                    */?>
                </div>
            </div>
            <?php endif; ?>
            <?php if (!empty($property_right)):?>
            <div class="col-1-2">
                <div class="row">
                    <?php  foreach ($property_right as $row): if ($row['is_show']): ?>
                    <div class="col-1-2 proprty-title"><?php echo $row['title'] ?>:</div>
                    <div class="col-1-2 property-value"><?php echo $row['value'] ?></div>
                    <?php endif; endforeach; ?>
                    <?/*
                    <div class="col-1-2 proprty-title">Емкость аккумулятора:</div>
                    <div class="col-1-2 property-value">630 мА⋅ч</div>
                    <div class="col-1-2 proprty-title">Интерфейсы:</div>
                    <div class="col-1-2 property-value">Bluetooth 1.2, USB</div>
                    <div class="col-1-2 proprty-title">Количество SIM-карт:</div>
                    <div class="col-1-2 property-value">2</div>
                    <div class="col-1-2 proprty-title">Количество ядер процессора:</div>
                    <div class="col-1-2 property-value">2</div>
                    <div class="col-1-2 proprty-title">Объем встроенной памяти:</div>
                    <div class="col-1-2 property-value">128 Мб</div>
                    <div class="col-1-2 proprty-title">Объем оперативной памяти:</div>
                    <div class="col-1-2 property-value">512 Мб</div>
                    */?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php endif; ?>
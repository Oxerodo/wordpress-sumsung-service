<?php if ($pricelist = get_field('table_pricelist')): ?>
<section class="pricelist">
    <div class="container">
        <div class="row">
            <div class="wrapper pricelist-wrapper">
                <?php if ($title = get_field('h2_pricelist_title')):?>
                <h2 class="title title-pricelist"><?php echo $title ?></h2>
                <?php else: ?>
                <h2 class="title title-pricelist">Цены на ремонт устройств Samsung</h2>
                <?php endif; ?>
                <p class="pricelist-data-updated">Дата обновления прайса: <?php echo current_time( 'd.m.Y');?></p>
                <table>
                    <thead>
                        <tr>
                            <td>Описание работы</td>
                            <td>Цена</td>
                            <td>Время</td>
                            <td>Заказать</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($pricelist as $row): ?>
                        <tr>
                            <td><?php echo $row['description']?></td>
                            <td><?php echo $row['price']?></td>
                            <td><?php echo $row['time']?></td>
                            <td>
                                <button type="button" class="btn btn-pricelist">Заказать</button>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pricelist-description-text">Цены указаны за работу мастера, без учета запчастей. Для расчета
                    точной стоимости ремонта свяжитесь с нашими операторами через форму заказа или позвоните по
                    контактному номеру телефона.</div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<section class="banner">
    <div class="container">
        <?php if (!is_front_page()) : ?>
            <div class="row">
                <div class="breadcrumbs wrapper">
                    <?php do_action('add_breadcrumbs'); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="wrapper banner-wrapper">
                <div class="d-flex flex-wrap">
                    <div class="section-banner">
                        <?php if (is_front_page()) : ?>
                            <?php if ($main_banner = get_field('main_banner')) : ?>
                                <h1 class="banner-title"><?php echo getWrapperSumsung($main_banner['main_title']) ?></h1>
                            <?php else : ?>
                                <h1 class="banner-title"><b>Ошибка!</b> Заполнител заголовок баннера на этой странице</h1>
                            <?php endif; ?>

                        <?php else : ?>
                            <?php if ($main_banner = get_field('main_banner')) : ?>
                                <h1 class="banner-title"><?php echo getWrapperSumsung($main_banner['main_title']) ?></h1>
                            <?php else : ?>
                                <h1 class="banner-title"><?php echo getWrapperSumsung(get_the_title()); ?></h1>
                            <?php endif; ?>
                        <?php endif; ?>

                        <div class="banner-text-wrapper">

                            <?php if (isset($main_banner)) : ?>
                                <?php echo $main_banner['main_text']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="banner-form-wrapper">
                            <?php echo do_shortcode('[contact-form-7 id="243" title="Форма в баннере" html_class="d-flex sm-flex-collumn" ]') ?>
                            <?/*
                            <form action="#" class="d-flex sm-flex-collumn">
                                <input type="tel" placeholder="Ваш телефон" class="input-banner-form-phone">
                                <button class="btn btn-banner-form-submit"><span>Заказать ремонт </span></button>
                            </form>
                            */?>
                        </div>
                    </div>
                    <div class="section-banner">
                        <div class="banner-image-wrapper">
                            <?php if (isset($main_banner)) : ?>
                                <?php $img = wp_get_attachment_image_url($main_banner['main_image_id'], 'sumsungservice-mainbanner'); ?>
                                <?php $alt = get_post_meta($main_banner['main_image_id'], '_wp_attachment_image_alt', true); ?>
                                <img src="<?php echo $img; ?>" alt="<?php echo $alt; ?>" class="banner-image">
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
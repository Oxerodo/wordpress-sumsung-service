<section class="advantages">
    <div class="container">
        <div class="row">
            <?php if ($title = get_field('h2_title_advantages', 'option')): ?>
            <h2 class="advantages-title"><?php echo $title; ?></h2>
            <?php endif; ?>
        </div>
        <?php if ($case = get_field('case_advantages', 'option')): ?>
        
        <div class="row">
            <div class="wrapper advantages-wrapper d-flex flex-wrap justify-content-between js-sm-slider-advantages">
                <?php foreach ($case as $item): ?>
                <div class="item-advantages">
                    <div class="image-advantages-wrapper">
                        <?php $img = wp_get_attachment_image_url($item['image-advantages-id'], 'full'); ?>
                        <?php $alt = get_post_meta($item['image-advantages-id'], '_wp_attachment_image_alt', true); ?>
                        <img src="<?php echo $img ?>" alt="<?php echo $alt ?>" class="image-advantages">
                    </div>
                    <div class="item-advatage-title"><?php echo getCityName($item['item-advatage-title']); ?></div>
                    <div class="item-advatage-description"><?php echo getCityName($item['item-advatage-description']); ?></div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endif;?>
    </div>
</section>
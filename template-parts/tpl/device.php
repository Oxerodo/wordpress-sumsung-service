<?php $categories = get_field('category'); ?>
<section class="type-device">
    <div class="container">
        <div class="row">
            <div class="wrapper type-device-wrapper d-flex flex-wrap">
                <pre>
                    <?php  //var_dump($categories); ?>
                </pre>
                    <h2 class="type-device-title">Выберите тип устройства</h2>
                <?php foreach ($categories as $category): ?>
                <?php $post = $category['page']; ?>
                <div class="card-categoty-device">
                    <div class="card-category-device-inner">
                        <div class="image-category-device-wrapper">
                        
                            <?php $img = get_the_post_thumbnail_url($post->ID, 'sumsungservice-devicepreview'); ?>
                            <img src="<?=$img?>" alt="фото: <?=$post->post_title?>" class="image-category-device">

                        </div>
                        
                        <?php if (empty($category['title'])):?>
                        <div class="card-category-device-title"><a href="<?=get_permalink($post->ID)?>"><?=$post->post_title?></a></div>
                        <?php else: ?>
                        <div class="card-category-device-title"><a href="<?=get_permalink($post->ID)?>"><?=$category['title']?></a></div>
                        <?php endif; ?>

                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<?php
$categoty_id = get_field('svyazannaya_rubrika');
query_posts('cat=' . $categoty_id . '&posts_per_page=20&order=ASC&orderby=title');
global $wp_query;
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$max_pages = $wp_query->max_num_pages;
?>
<section id="type_device" class="type-device">
    <div class="container">
        <div class="row">
            <div class="wrapper type-device-wrapper d-flex flex-wrap">
                <h2 class="type-device-title type-device-select-title">Выберите тип устройства</h2>
                <div class="type-device-select-wrapper">
                    <div class="type-device-select-inner">
                        <select class="type-device-select-list">
                            <option value="#" disabled>Найти модель</option>
                            <?php while (have_posts()) : the_post(); ?>
                                <option value="<?php the_ID() ?>"><?php the_title() ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                </div>
                <? while (have_posts()) : the_post(); ?>
                    <div class="card-categoty-device">
                        <div class="card-category-device-inner">
                            <div class="image-category-device-wrapper">
                                <?php $img = get_the_post_thumbnail_url(get_the_ID(), 'sumsungservice-devicepreview'); ?>
                                <img src="<?= $img ?>" alt="фото: <?php the_title() ?>" class="image-category-device">
                            </div>
                            <div class="card-category-device-title"><a href="<?= get_permalink(get_the_ID()) ?>"><?php the_title() ?></a></div>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php if( $paged < $max_pages ): ?>
                <button class="btn btn-category-device-more js-btn-category-device-show-more" data-max="<?=$max_pages?>" data-current="<?=$paged?>">Показать еще</button>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>
<?php wp_reset_query(); ?>
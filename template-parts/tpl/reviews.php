<section class="reviews">
    <div class="container">
        <div class="row">
            <div class="wrapper">
                <h2 class="title title-reviews">Отзывы наших клиентов</h2>
                <div class="wrapper-button-new-reviews">
                    <button class="btn btn-new-reviews">Написать отзыв</button>
                </div>
                <div class="review-card">
                    <div class="review-card-header d-flex justify-content-between align-items-end">
                        <div class="review-card-date">Дата: 15.20.2021</div>
                        <div class="review-card-rating">
                            <div class="reviews-card-rating-status"></div>
                            <div class="reviews-card-rating-status empty"></div>
                            <div class="reviews-card-rating-status empty"></div>
                            <div class="reviews-card-rating-status empty"></div>
                            <div class="reviews-card-rating-status empty"></div>
                        </div>
                    </div>
                    <div class="review-card-content">
                        <div class="review-card-name">Ершова Софья</div>
                        <div class="review-card-text">
                            <p>У меня уже довольно старый телефон – Samsung Galaxy J7, поэтому неудивительно что батарея
                                подсела. Я подумала что надо ее менять, но заниматься всем этим самой мне некогда.
                                Оставила заявку на сайте, я ж еще и скидку за это получу. По ней перезвонили, сказали
                                ждать курьера. Тот приехал быстро и увез телефон. </p>
                        </div>
                    </div>
                </div>
                <div class="review-card">
                    <div class="review-card-header d-flex justify-content-between align-items-end">
                        <div class="review-card-date">Дата: 15.20.2021</div>
                        <div class="review-card-rating">
                            <div class="reviews-card-rating-status"></div>
                            <div class="reviews-card-rating-status empty"></div>
                            <div class="reviews-card-rating-status empty"></div>
                            <div class="reviews-card-rating-status empty"></div>
                            <div class="reviews-card-rating-status empty"></div>
                        </div>
                    </div>
                    <div class="review-card-content">
                        <div class="review-card-name">Ершова Софья</div>
                        <div class="review-card-text">
                            <p>У меня уже довольно старый телефон – Samsung Galaxy J7, поэтому неудивительно что батарея
                                подсела. Я подумала что надо ее менять, но заниматься всем этим самой мне некогда.
                                Оставила заявку на сайте, я ж еще и скидку за это получу. По ней перезвонили, сказали
                                ждать курьера. Тот приехал быстро и увез телефон. </p>
                        </div>
                    </div>
                </div>
                <div class="review-card">
                    <div class="review-card-header d-flex justify-content-between align-items-end">
                        <div class="review-card-date">Дата: 15.20.2021</div>
                        <div class="review-card-rating">
                            <div class="reviews-card-rating-status"></div>
                            <div class="reviews-card-rating-status empty"></div>
                            <div class="reviews-card-rating-status empty"></div>
                            <div class="reviews-card-rating-status empty"></div>
                            <div class="reviews-card-rating-status empty"></div>
                        </div>
                    </div>
                    <div class="review-card-content">
                        <div class="review-card-name">Ершова Софья</div>
                        <div class="review-card-text">
                            <p>У меня уже довольно старый телефон – Samsung Galaxy J7, поэтому неудивительно что батарея
                                подсела. Я подумала что надо ее менять, но заниматься всем этим самой мне некогда.
                                Оставила заявку на сайте, я ж еще и скидку за это получу. По ней перезвонили, сказали
                                ждать курьера. Тот приехал быстро и увез телефон. </p>
                        </div>
                    </div>
                </div>
                <div class="pagination d-flex justify-content-center">
                    <a class="pagination-link" href="#">
                        <div class="pagination-button pagination-first-button">Первая</div>
                    </a>
                    <a class="pagination-link" href="#">
                        <div class="pagination-button pagination-prev-button"></div>
                    </a>
                    <div class="pagination-button pagination-page-button active">1</div>
                    <a class="pagination-link" href="#">
                        <div class="pagination-button pagination-page-button">2</div>
                    </a>
                    <a class="pagination-link" href="#">
                        <div class="pagination-button pagination-page-button">3</div>
                    </a>
                    <a class="pagination-link" href="#">
                        <div class="pagination-button pagination-next-button"></div>
                    </a>
                    <a class="pagination-link" href="#">
                        <div class="pagination-button pagination-last-button">Последняя</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="review-form">
	<div class="container">
		<div class="row">
			<div class="wrapper">
				<h2 class="title title-reviews-from">Написать отзыв</h2>
				<form action="#" id="revies-from" class="form-reviews row flex-wrap wrapper-offset-45">
					<div class="col-1-3 offset-45">
						<p class="form-field-name">Ваше имя</p>
						<input type="text" class="input-form form-input-name" name="name" placeholder="Укажите ваше имя">
					</div>
					<div class="col-1-3 offset-45">
						<p class="form-field-name">Номер договора</p>
						<input type="text" class="input-form form-input-name" name="name" placeholder="Укажите ваше имя">
					</div>
					<div class="col-1-3 offset-45">
						<p class="form-field-name">Оценка</p>
						<div class="input-score-wrapper">
							<div data-rating="1" class="input-score-item"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M15 1.61804L17.8922 10.5193L18.0044 10.8647H18.3677H27.727L20.1552 16.366L19.8613 16.5795L19.9735 16.925L22.8657 25.8262L15.2939 20.325L15 20.1115L14.7061 20.325L7.13428 25.8262L10.0265 16.925L10.1387 16.5795L9.84482 16.366L2.27299 10.8647H11.6323H11.9956L12.1078 10.5193L15 1.61804Z" stroke="#0000A0" />
								</svg>
							</div>
							<div data-rating="2" class="input-score-item"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M15 1.61804L17.8922 10.5193L18.0044 10.8647H18.3677H27.727L20.1552 16.366L19.8613 16.5795L19.9735 16.925L22.8657 25.8262L15.2939 20.325L15 20.1115L14.7061 20.325L7.13428 25.8262L10.0265 16.925L10.1387 16.5795L9.84482 16.366L2.27299 10.8647H11.6323H11.9956L12.1078 10.5193L15 1.61804Z" stroke="#0000A0" />
								</svg>
							</div>
							<div data-rating="3" class="input-score-item"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M15 1.61804L17.8922 10.5193L18.0044 10.8647H18.3677H27.727L20.1552 16.366L19.8613 16.5795L19.9735 16.925L22.8657 25.8262L15.2939 20.325L15 20.1115L14.7061 20.325L7.13428 25.8262L10.0265 16.925L10.1387 16.5795L9.84482 16.366L2.27299 10.8647H11.6323H11.9956L12.1078 10.5193L15 1.61804Z" stroke="#0000A0" />
								</svg>
							</div>
							<div data-rating="4" class="input-score-item"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M15 1.61804L17.8922 10.5193L18.0044 10.8647H18.3677H27.727L20.1552 16.366L19.8613 16.5795L19.9735 16.925L22.8657 25.8262L15.2939 20.325L15 20.1115L14.7061 20.325L7.13428 25.8262L10.0265 16.925L10.1387 16.5795L9.84482 16.366L2.27299 10.8647H11.6323H11.9956L12.1078 10.5193L15 1.61804Z" stroke="#0000A0" />
								</svg>
							</div>
							<div data-rating="5" class="input-score-item"><svg width="30" height="28" viewBox="0 0 30 28" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M15 1.61804L17.8922 10.5193L18.0044 10.8647H18.3677H27.727L20.1552 16.366L19.8613 16.5795L19.9735 16.925L22.8657 25.8262L15.2939 20.325L15 20.1115L14.7061 20.325L7.13428 25.8262L10.0265 16.925L10.1387 16.5795L9.84482 16.366L2.27299 10.8647H11.6323H11.9956L12.1078 10.5193L15 1.61804Z" stroke="#0000A0" />
								</svg>
							</div>
						</div>
					</div>
					<div class="col-1-3 offset-45">
						<p class="form-field-name">Ваш телефон</p>
						<input type="text" class="input-form form-input-name" name="name" placeholder="Укажите ваше имя">
					</div>
					<div class="col-1-3 offset-45">
						<p class="form-field-name">Ваш email</p>
						<input type="text" class="input-form form-input-name" name="name" placeholder="Укажите ваше имя">
					</div>
					<input type="hidden" name="">
					<div class="col-1-3 offset-45">
						<p class="form-field-name">Услуга</p>
						<div class="input-select-wrapper">
							<div class="input-select-inner">
								<!-- #TODO: доделать скролл чтобы был как в макете -->
								<select class="input-select-list">
									<option value="#">Выберите услугу</option>
									<option value="#">Услуга 1</option>
									<option value="#">Услуга 2</option>
									<option value="#">Услуга 3</option>
									<option value="#">Услуга 4</option>
									<option value="#">Услуга 5</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-1-1 offset-45">
						<textarea class="input-textarea" name="massage" id="massage" cols="30" rows="8" placeholder="Текст отзыва"></textarea>
						<button type="submit" class="btn btn-submit">Оставить отзыв</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
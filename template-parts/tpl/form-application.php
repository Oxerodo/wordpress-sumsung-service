<style>
    .form-application-wrapper {
        background-color: #0000A0;
        width: 455px;
        height: 554px;
        padding: 25px;
    }

    .form-application-close-wrapper {
        height: 25px;
        width: 25px;
        margin-bottom: 20px;
        transition: transform 0.5s;
        cursor: pointer;
    }

    .form-application-close-wrapper:hover {
        transform: rotate(180deg);
    }

    .form-application-title {
        font-family: 'Samsung Sharp Sans';
        font-style: normal;
        font-weight: bold;
        font-size: 30px;
        line-height: 39px;
        text-align: center;
        color: #FFFFFF;
        text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
        margin-bottom: 50px;
    }

    .input-application-label {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: normal;
        font-size: 18px;
        line-height: 25px;
        /* identical to box height */
        text-align: center;
        color: #FFFFFF;
        margin-bottom: 8px;
    }

    .input-application-input {
        width: 360px;
        height: 50px;
        border: solid 1px #D9D9D9;
        background-color: white;
        padding: 0 13px;
        font-size: 15px;
        line-height: 20px;
    }

    .input-application-submit {
        cursor: pointer;
        width: 360px;
        height: 50px;
        border: solid 1px #D9D9D9;
        background-color: white;
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: normal;
        font-size: 18px;
        line-height: 25px;

        /* identical to box height */
        margin-top: 10px;

        color: #000000;
    }

    .form-application-group {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        padding-left: 20px;
        padding-right: 20px;
        margin-bottom: 30px;

    }

    .paralax {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 9999px;
        right: 0;
        background-color: #efefef;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: -9999;
        opacity: 0;
        transition: opacity 0.3s;
    }

    .paralax.show {
        left: 0;
        z-index: 9999;
        opacity: 1;
    }
</style>
<div class="paralax js-modalFormApplication">
    <?php echo do_shortcode('[contact-form-7 id="258" title="Оставить заявку" html_class="form-application-wrapper"]') ?>
    <?php /*
    <form class="form-application-wrapper">
        <div class="form-application-close-wrapper js-form-application-close-wrapper">
            <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 1L26 26" stroke="white" />
                <path d="M26 1L1 26" stroke="white" />
            </svg>

        </div>
        <div class="form-application-title">Оставьте заявку <br>и мы с вами свяжемся</div>
        <div class="form-application-group">
            <label class="input-application-label" for="input-application-name">Ваше имя</label>
            <input type="text" id="input-application-name" class="input-application-input" placeholder="Введите ваше имя">
        </div>
        <div class="form-application-group">
            <label class="input-application-label" for="input-application-phone">Ваш телефон</label>
            <input type="text" id="input-application-phone" class="input-application-input" placeholder="+ 7 (999) 999 99-99">
        </div>
        <div class="form-application-group">
            <input type="submit" id="input-application-submit" class="input-application-submit">
        </div>
    </form>
    */?>
</div>


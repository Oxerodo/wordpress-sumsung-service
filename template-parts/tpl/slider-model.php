<section class="slider-model">
    <div class="container">
        <div class="wrapper">
            <h2 class="title title-slider-model">Сейчас в ремонте</h2>
            <div class="js-slider slider-model row wrapper-offset-20">
                <div class="card-categoty-device col-1-4 offset-20">
                    <div class="card-category-device-inner">
                        <div class="image-category-device-wrapper">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/smartphone.png" alt="Смартфон" class="image-category-device">
                        </div>
                        <div class="card-category-device-title">Ремонт смартфонов</div>
                    </div>
                </div>
                <div class="card-categoty-device col-1-4 offset-20">
                    <div class="card-category-device-inner">
                        <div class="image-category-device-wrapper">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/smartphone.png" alt="Смартфон" class="image-category-device">
                        </div>
                        <div class="card-category-device-title">Ремонт смартфонов</div>
                    </div>
                </div>
                <div class="card-categoty-device col-1-4 offset-20">
                    <div class="card-category-device-inner">
                        <div class="image-category-device-wrapper">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/smartphone.png" alt="Смартфон" class="image-category-device">
                        </div>
                        <div class="card-category-device-title">Ремонт смартфонов</div>
                    </div>
                </div>
                <div class="card-categoty-device col-1-4 offset-20">
                    <div class="card-category-device-inner">
                        <div class="image-category-device-wrapper">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/smartphone.png" alt="Смартфон" class="image-category-device">
                        </div>
                        <div class="card-category-device-title">Ремонт смартфонов</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
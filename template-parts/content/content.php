<?php if (isset($args['class'])) {
    $class = ' ' . $args['class'];
} else {
    $class = '';
}
?>
<section class="content<?php echo $class ?>">
    <div class="container">
        <div class="row">
            <div class="wrapper content-wrapper">
                <?php the_content() ?>
            </div>
        </div>
    </div>
</section>
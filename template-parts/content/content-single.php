<?php if (isset($args['class'])) {
    $class = ' ' . $args['class'];
} else {
    $class = '';
}
query_posts('page_id='.get_field('page_category'));
while( have_posts() ){ the_post();
?>
    <section class="content<?php echo $class ?>">
        <div class="container">
            <div class="row">
                <div class="wrapper content-wrapper">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>
<?php } wp_reset_query();?>
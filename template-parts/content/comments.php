<section id="comments" class="reviews">
	<div class="container">
		<div class="row">
			<div class="wrapper">
				<h2 class="title title-reviews">Отзывы наших клиентов</h2>
				<div class="wrapper-button-new-reviews">
					<button class="btn btn-new-reviews">Написать отзыв</button>
				</div>
				<?php comments_template(); ?>
			</div>
		</div>
	</div>
</section>
<?php
$commenter = wp_get_current_commenter();
$fields = array(
	'author' => '<div class="col-1-3 offset-45">
                    <p class="form-field-name">Ваше имя</p>
                    <input type="text" class="input-form form-input-name" name="author" value="' . esc_attr($commenter['comment_author']) .
		'" placeholder="Укажите ваше имя">
                </div>',
	'number' => '<div class="col-1-3 offset-45">
                    <p class="form-field-name">Номер договора</p>
                    <input type="text" class="input-form form-input-name" name="number" placeholder="Укажите номер договора">
                </div>',
	'rating' => '<div class="col-1-3 offset-45">
                <p class="form-field-name">Оценка</p>
                    <div class="input-score-wrapper">
                        <div data-rating="1" class="input-score-item full"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15 1.61804L17.8922 10.5193L18.0044 10.8647H18.3677H27.727L20.1552 16.366L19.8613 16.5795L19.9735 16.925L22.8657 25.8262L15.2939 20.325L15 20.1115L14.7061 20.325L7.13428 25.8262L10.0265 16.925L10.1387 16.5795L9.84482 16.366L2.27299 10.8647H11.6323H11.9956L12.1078 10.5193L15 1.61804Z" stroke="#0000A0" />
                            </svg>
                        </div>
                        <div data-rating="2" class="input-score-item"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15 1.61804L17.8922 10.5193L18.0044 10.8647H18.3677H27.727L20.1552 16.366L19.8613 16.5795L19.9735 16.925L22.8657 25.8262L15.2939 20.325L15 20.1115L14.7061 20.325L7.13428 25.8262L10.0265 16.925L10.1387 16.5795L9.84482 16.366L2.27299 10.8647H11.6323H11.9956L12.1078 10.5193L15 1.61804Z" stroke="#0000A0" />
                            </svg>
                        </div>
                        <div data-rating="3" class="input-score-item"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15 1.61804L17.8922 10.5193L18.0044 10.8647H18.3677H27.727L20.1552 16.366L19.8613 16.5795L19.9735 16.925L22.8657 25.8262L15.2939 20.325L15 20.1115L14.7061 20.325L7.13428 25.8262L10.0265 16.925L10.1387 16.5795L9.84482 16.366L2.27299 10.8647H11.6323H11.9956L12.1078 10.5193L15 1.61804Z" stroke="#0000A0" />
                            </svg>
                        </div>
                        <div data-rating="4" class="input-score-item"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15 1.61804L17.8922 10.5193L18.0044 10.8647H18.3677H27.727L20.1552 16.366L19.8613 16.5795L19.9735 16.925L22.8657 25.8262L15.2939 20.325L15 20.1115L14.7061 20.325L7.13428 25.8262L10.0265 16.925L10.1387 16.5795L9.84482 16.366L2.27299 10.8647H11.6323H11.9956L12.1078 10.5193L15 1.61804Z" stroke="#0000A0" />
                            </svg>
                        </div>
                        <div data-rating="5" class="input-score-item"><svg width="30" height="28" viewBox="0 0 30 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15 1.61804L17.8922 10.5193L18.0044 10.8647H18.3677H27.727L20.1552 16.366L19.8613 16.5795L19.9735 16.925L22.8657 25.8262L15.2939 20.325L15 20.1115L14.7061 20.325L7.13428 25.8262L10.0265 16.925L10.1387 16.5795L9.84482 16.366L2.27299 10.8647H11.6323H11.9956L12.1078 10.5193L15 1.61804Z" stroke="#0000A0" />
                            </svg>
                        </div>
                    </div>
					<input type="hidden" class="rating-value" name="rating" value="1">
                </div>',
	'phone' => '<div class="col-1-3 offset-45">
					<p class="form-field-name">Ваш телефон</p>
					<input type="text" class="input-form form-input-name" name="phone" placeholder="Укажите ваше имя">
				</div>',
	'email' => '<div class="col-1-3 offset-45">
                    <p class="form-field-name">Ваш E-mail</p>
                    <input type="text" class="input-form form-input-name" name="email" placeholder="Укажите ваш E-mail">
                </div>',
	'servce-id' => '<div class="col-1-3 offset-45">
                    <p class="form-field-name">Услуга</p>
                    <input type="text" class="input-form form-input-name" name="servce-id-name" value="' . esc_html(get_the_title(get_the_ID())) . '" placeholder="Укажите ваш E-mail">
					<input type="hidden" name="servce-id" value="' . get_the_ID() . '">
                </div>',
	'cookies' => ''
);
$comment_field = '<div class="col-1-1 offset-45"><textarea class="input-textarea" name="comment" id="comment" cols="30" rows="8" placeholder="Текст отзыва"></textarea></div>';
$comments_args = array(
	'fields' => $fields,
	'comment_field' => $comment_field,
	'title_reply' => 'Написать отзыв',
	'label_submit' => 'Оставить отзыв',
	'comment_notes_before' => '',
	'class_form' => 'form-reviews row flex-wrap wrapper-offset-45',
	'class_container' => 'wrapper',
	'class_submit' => 'btn btn-submit js-newReviewsFormSubmit',
	'submit_field' => '<div class="col-1-1 offset-45">%1$s %2$s</div>',
	'title_reply_before' => '<h2 class="title title-reviews-from">',
	'title_reply_after' => '</h2>'
)
?>
<div id="js-NewReviewsForm" class="parallax">
	<div id="js-closeNewReviewsForm" class="parallax-close">
		<span></span><span></span>
	</div>
	<section class="review-form">
		<div class="container">
			<div class="row">
				<?php comment_form($comments_args); ?>
			</div>
		</div>
	</section>
</div>
var btnNewReviewsForm = document.querySelector('.btn-new-reviews'),
	parallaxNewReviewsForm = document.querySelector('#js-NewReviewsForm'),
	btnCloseNewReviewsForm = document.querySelector('#js-closeNewReviewsForm'),
	btnNewReviewsFormSubmit = document.querySelector('.js-newReviewsFormSubmit');

if (btnNewReviewsForm !== null) {
	btnNewReviewsForm.addEventListener('click', function () {
		if (parallaxNewReviewsForm.classList.contains('show')) {
			parallaxNewReviewsForm.classList.remove('show');
		} else {
			parallaxNewReviewsForm.classList.add('show');
		}

	});
	btnCloseNewReviewsForm.addEventListener('click', function () {
		if (parallaxNewReviewsForm.classList.contains('show')) {
			parallaxNewReviewsForm.classList.remove('show');
		} else {
			parallaxNewReviewsForm.classList.add('show');
		}
	});
}
//JS Валидация формы обратной связи

var phoneFields = document.querySelectorAll('.wpcf7-validates-as-tel');
phoneFields.forEach(phoneField => phoneField.oninput = function () {
	phoneMaskValidation(phoneField);
});
var form = document.querySelector('#commentform');
if (form !== null) {
	var author = form.querySelector('[name="author"]'),
		number = form.querySelector('[name="number"]'),
		rating = form.querySelector('[name="rating"]'),
		phone = form.querySelector('[name="phone"]'),
		email = form.querySelector('[name="email"]'),
		comment = form.querySelector('#comment'),
		fields = form.querySelectorAll('.form-input-name');

	// console.log(fields);
	if (author !== null) {
		fields.forEach(field => field.oninput = function () {
			field.classList.remove('error-validation');
		});


		phone.oninput = function () {
			phoneMaskValidation(phone);
		}


		email.oninput = function () {
			comment.classList.remove('error-validation');
		}
		email.oninput = function () {
			if (validateEmail(email.value)) {
				email.classList.remove('error-validation');
			} else {
				email.classList.add('error-validation');
			}
		}


		number.oninput = function () {
			if (validateNumber(number.value)) {
				number.classList.remove('error-validation');
			} else {
				number.classList.add('error-validation');
			}
		}
		function validateNumber(number) {
			const re = /^[0-9]{0,20}[^\s][0-9]{0,20}[^\s][0-9]{0,20}[^\s][0-9]{0,20}$/;
			return re.test(String(number).toLowerCase());
		}
		function validateEmail(email) {
			const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
		}

		btnNewReviewsFormSubmit.addEventListener('click', function (e) {
			// e.preventDefault();
			let status = true;
			if (author.value === '') {
				status = false;
				author.classList.add('error-validation');
			}
			if (number.value === '') {
				status = false;
				number.classList.add('error-validation');
			}
			if (rating.value <= 0 || rating.value >= 5) {
				status = false;
			}
			if (phone.value === '') {
				status = false;
				phone.classList.add('error-validation');
			}
			if (email.value === '') {
				status = false;
				email.classList.add('error-validation');
			}
			if (comment.value === '') {
				status = false;
				comment.classList.add('error-validation');
			}
			if (!status) {
				e.preventDefault();
				return false;
			}
			console.log('status', status);
		});

		var rating = document.querySelector('.input-score-wrapper'),
			ratingItem = document.querySelectorAll('.input-score-item'),
			raingValue = document.querySelector('.rating-value');

		rating.onclick = function (e) {
			var target = e.target;
			if (target.classList.contains('input-score-item')) {
				removeClass(ratingItem, 'current-active')
				target.classList.add('full', 'current-active');
				// console.log(target.dataset.rating);
				raingValue.value = target.dataset.rating;
				// console.log('raingValue.value', raingValue.value);
			}
		}

		rating.onmouseover = function (e) {
			var target = e.target;
			if (target.classList.contains('input-score-item')) {
				removeClass(ratingItem, 'full')
				target.classList.add('full');
				mouseOverActiveClass(ratingItem)
			}
		}
		rating.onmouseout = function () {
			addClass(ratingItem, 'full');
			mouseOutActiveClas(ratingItem);
		}

		function removeClass(arr) {
			for (var i = 0, iLen = arr.length; i < iLen; i++) {
				for (var j = 1; j < arguments.length; j++) {
					ratingItem[i].classList.remove(arguments[j]);
				}
			}
		}
		function addClass(arr) {
			for (var i = 0, iLen = arr.length; i < iLen; i++) {
				for (var j = 1; j < arguments.length; j++) {
					ratingItem[i].classList.add(arguments[j]);
				}
			}
		}

		function mouseOverActiveClass(arr) {
			for (var i = 0, iLen = arr.length; i < iLen; i++) {
				if (arr[i].classList.contains('full')) {
					break;
				} else {
					arr[i].classList.add('full');
				}
			}
		}

		function mouseOutActiveClas(arr) {
			for (var i = arr.length - 1; i >= 1; i--) {
				if (arr[i].classList.contains('current-active')) {
					break;
				} else {
					arr[i].classList.remove('full');
				}
			}
		}
	}
}
// var btnNewReviewsForm = document.querySelector('.btn-new-reviews'),
//     parallaxNewReviewsForm = document.querySelector('#js-NewReviewsForm');

// btnNewReviewsForm.addEventListener('click', function(){
//   if (parallaxNewReviewsForm.classList.contains('show')){
//     parallaxNewReviewsForm.classList.remove('show');
//   }else{
//     parallaxNewReviewsForm.classList.add('show');
//   }

// });

/*
* this get querySelector
*/
function phoneMaskValidation(input) {
	let mask = '+7 (000) 000-00-00',
		// pattern = '\\+7 ([0-9]{3}\) [0-9]{3}[\-][0-9]{2}[\-][0-9]{2}',
		literalPattern = /[0\*]/,
		numberPattern = /[0-9]/,
		newValue = "";
	try {
		let maskLength = mask.length,
			valueIndex = 0,
			maskIndex = 0;
		for (; maskIndex < maskLength;) {
			if (maskIndex >= input.value.length) break;
			if (mask[maskIndex] === "0" && input.value[valueIndex].match(numberPattern) === null) break;

			while (mask[maskIndex].match(literalPattern) === null) {
				if (input.value[valueIndex] === mask[maskIndex]) break;
				newValue += mask[maskIndex++];
			}
			newValue += input.value[valueIndex++];
			maskIndex++;
		}
		input.value = newValue;
	}

	catch (e) {
		console.log(e);
	}
}
var buttonsApplication = document.querySelectorAll('.js-openModalFormApllication'),
	modalApp = document.querySelector('.js-modalFormApplication');
buttonsApplication.forEach(btn => btn.onclick = function () {
	modalApp.classList.add('show');
});
document.querySelector('.js-form-application-close-wrapper').onclick = function () {
	modalApp.classList.remove('show');
};
var buttonschecked = document.querySelectorAll('.js-openModalFormChecked'),
	modalCheck = document.querySelector('.js-modalFormChecked');
buttonschecked.forEach(btn => btn.onclick = function () {
	modalCheck.classList.add('show');
});
document.querySelector('.js-form-checked-close-wrapper').onclick = function () {
	modalCheck.classList.remove('show');
};
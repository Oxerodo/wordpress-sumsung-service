<?php

/**
 * Расширение комментарие
 * Добавляю поле рейтингов
 */
// add_filter('comment_form_default_fields', 'extend_comment_custom_default_fields');
// function extend_comment_custom_default_fields($fields)
// {

// 	$commenter = wp_get_current_commenter();
// 	$req = get_option('require_name_email');
// 	$aria_req = ($req ? " aria-required='true'" : '');

// 	$fields['author'] = '<p class="comment-form-author">' .
// 		'<label for="author">' . __('Name') . '</label>' .
// 		($req ? '<span class="required">*</span>' : '') .
// 		'<input id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) .
// 		'" size="30" tabindex="1"' . $aria_req . ' /></p>';

// 	$fields['email'] = '<p class="comment-form-email">' .
// 		'<label for="email">' . __('Email') . '</label>' .
// 		($req ? '<span class="required">*</span>' : '') .
// 		'<input id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) .
// 		'" size="30"  tabindex="2"' . $aria_req . ' /></p>';

// 	$fields['url'] = '<p class="comment-form-url">' .
// 		'<label for="url">' . __('Website') . '</label>' .
// 		'<input id="url" name="url" type="text" value="' . esc_attr($commenter['comment_author_url']) .
// 		'" size="30"  tabindex="3" /></p>';

// 	$fields['phone'] = '<p class="comment-form-phone">' .
// 		'<label for="phone">' . __('Phone') . '</label>' .
// 		'<input id="phone" name="phone" type="text" size="30"  tabindex="4" /></p>';

// 	$fields['number'] = '<p class="comment-form-number">' .
// 		'<label for="number">' . __('Number') . '</label>' .
// 		'<input id="number" name="number" type="text" size="30"  tabindex="4" /></p>';

// 	$fields['servce-id'] = '<p class="comment-form-servce-id">' .
// 		'<label for="servce-id">' . __('Service_ID') . '</label>' .
// 		'<input id="servce-id" name="servce-id" type="text" size="30"  tabindex="4" /></p>';

// 	return $fields;
// }

// Добавляем поля для всех пользователей
add_action('comment_form_logged_in_after', 'extend_comment_custom_fields');
add_action('comment_form_after_fields', 'extend_comment_custom_fields');
function extend_comment_custom_fields()
{


	// echo '<p class="comment-form-rating">' .
	// 	'<label for="rating">' . __('Rating') . '<span class="required">*</span></label>
	// 		  <span class="commentratingbox">';

	// for ($i = 1; $i <= 5; $i++) {
	// 	echo '
	// 	<label class="commentrating" style="display:inline-block;">
	// 		<input type="radio" name="rating" id="rating" value="' . $i . '"/> ' . $i . '   
	// 	</label>';
	// }

	// echo '</span></p>';
}
// Сохраняем данные комментариев
add_action('comment_post', 'save_extend_comment_meta_data');
function save_extend_comment_meta_data($comment_id)
{

	if (!empty($_POST['phone'])) {
		$phone = sanitize_text_field($_POST['phone']);
		add_comment_meta($comment_id, 'phone', $phone);
	}
	if (!empty($_POST['number'])) {
		$number = sanitize_text_field($_POST['number']);
		add_comment_meta($comment_id, 'number', $number);
	}
	if (!empty($_POST['servce_id'])) {
		$servce_id = sanitize_text_field($_POST['servce_id']);
		add_comment_meta($comment_id, 'servce_id', $servce_id);
	}


	if (!empty($_POST['rating'])) {
		$rating = intval($_POST['rating']);
		add_comment_meta($comment_id, 'rating', $rating);
	}
}

// Проверяем, заполнено ли поле "Рейтинг"
add_filter('preprocess_comment', 'verify_extend_comment_meta_data');
function verify_extend_comment_meta_data($commentdata)
{

	// ничего не делаем если это ответ на комментарий
	if (isset($_REQUEST['action']) && $_REQUEST['action'] === 'replyto-comment') {
		return $commentdata;
	}

	if (empty($_POST['rating']) || !(int)$_POST['rating']) {
		wp_die(__('Error: You did not add a rating. Hit the Back button on your Web browser and resubmit your comment with a rating.'));
	}

	return $commentdata;
}

// Отображение содержимого метаполей во фронт-энде
add_filter('comment_text', 'modify_extend_comment');
function modify_extend_comment($text)
{
	global $post;

	if ($commenttitle = get_comment_meta(get_comment_ID(), 'title', true)) {
		$commenttitle = '<strong>' . esc_attr($commenttitle) . '</strong><br/>';
		$text = $commenttitle . $text;
	}

	if ($commentrating = get_comment_meta(get_comment_ID(), 'rating', true)) {

		$commentrating = wp_star_rating(array(
			'rating' => $commentrating,
			'echo' => false
		));

		$text = $text . $commentrating;
	}

	return $text;
}

add_action('wp_enqueue_scripts', 'check_count_extend_comments');
function check_count_extend_comments()
{
	global $post;

	if (isset($post) && (int)$post->comment_count > 0) {
		require_once ABSPATH . 'wp-admin/includes/template.php';
		add_action('wp_enqueue_scripts', function () {
			wp_enqueue_style('dashicons');
		});

		$stars_css = '
		.star-rating .star-full:before { content: "\f155"; }
		.star-rating .star-empty:before { content: "\f154"; }
		.star-rating .star {
			color: #0074A2;
			display: inline-block;
			font-family: dashicons;
			font-size: 20px;
			font-style: normal;
			font-weight: 400;
			height: 20px;
			line-height: 1;
			text-align: center;
			text-decoration: inherit;
			vertical-align: top;
			width: 20px;
		}
		';

		wp_add_inline_style('dashicons', $stars_css);
	}
}

// Добавляем новый метабокс на страницу редактирования комментария
add_action('add_meta_boxes_comment', 'extend_comment_add_meta_box');
function extend_comment_add_meta_box()
{
	add_meta_box('title', __('Comment Metadata - Extend Comment'), 'extend_comment_meta_box', 'comment', 'normal', 'high');
}

// Отображаем наши поля
function extend_comment_meta_box($comment)
{
	$phone  = get_comment_meta($comment->comment_ID, 'phone', true);
	$number  = get_comment_meta($comment->comment_ID, 'number', true);
	$servce_id  = get_comment_meta($comment->comment_ID, 'servce_id', true);
	$rating = get_comment_meta($comment->comment_ID, 'rating', true);
	wp_nonce_field('extend_comment_update', 'extend_comment_update', false);
?>
	<p>
		<label for="phone"><?php _e('Phone'); ?></label>
		<input type="text" name="phone" value="<?php echo esc_attr($phone); ?>" class="widefat" />
	</p>
	<p>
		<label for="number"><?php _e('Number'); ?></label>
		<input type="text" name="number" value="<?php echo esc_attr($number); ?>" class="widefat" />
	</p>
	<p>
		<label for="servce_id"><?php _e('Servce-Id'); ?></label>
		<input type="text" name="servce_id" value="<?php echo esc_attr($servce_id); ?>" class="widefat" />
	</p>
	<p>
		<label for="rating"><?php _e('Rating: '); ?></label>
		<span class="commentratingbox">
			<?php
			for ($i = 0; $i <= 5; $i++) {
				echo '
		  <span class="commentrating">
			<input type="radio" name="rating" id="rating" value="' . $i . '" ' . checked($i, $rating, 0) . '/>
		  </span>';
			}
			?>
		</span>
	</p>
<?php
}
add_action('edit_comment', 'extend_comment_edit_meta_data');
function extend_comment_edit_meta_data($comment_id)
{
	if (!isset($_POST['extend_comment_update']) || !wp_verify_nonce($_POST['extend_comment_update'], 'extend_comment_update'))
		return;

	if (!empty($_POST['phone'])) {
		$phone = sanitize_text_field($_POST['phone']);
		update_comment_meta($comment_id, 'phone', $phone);
	} else
		delete_comment_meta($comment_id, 'phone');
	if (!empty($_POST['number'])) {
		$number = sanitize_text_field($_POST['number']);
		update_comment_meta($comment_id, 'number', $number);
	} else
		delete_comment_meta($comment_id, 'number');
	if (!empty($_POST['servce_id'])) {
		$servce_id = sanitize_text_field($_POST['servce_id']);
		update_comment_meta($comment_id, 'servce_id', $servce_id);
	} else
		delete_comment_meta($comment_id, 'servce_id');

	if (!empty($_POST['rating'])) {
		$rating = intval($_POST['rating']);
		update_comment_meta($comment_id, 'rating', $rating);
	} else
		delete_comment_meta($comment_id, 'rating');
}

function remove_comment_fields($fields) {
    unset($fields['url']);
    // unset($fields['comment']);
    return $fields;
}
add_filter('comment_form_default_fields','remove_comment_fields');

add_filter('comment_form_fields', 'kama_reorder_comment_fields' );
function kama_reorder_comment_fields( $fields ){
	// die(print_r( $fields )); // посмотрим какие поля есть

	$new_fields = array(); // сюда соберем поля в новом порядке

	$myorder = array('author', 'number', 'rating', 'phone','email', 'servce-id' ,'comment'); // нужный порядок

	foreach( $myorder as $key ){
		$new_fields[ $key ] = $fields[ $key ];
		unset( $fields[ $key ] );
	}

	// если остались еще какие-то поля добавим их в конец
	if( $fields )
		foreach( $fields as $key => $val )
			$new_fields[ $key ] = $val;

	return $new_fields;
}


function themeCommentsViewList($comment, $args, $depth){
	$rating = get_comment_meta($comment->comment_ID, 'rating', true);
?>
	<div class="review-card">
	<div class="review-card-header d-flex justify-content-between align-items-end">
		<div class="review-card-date">Дата: <?php comment_date('d.m.Y')?></div>
		<div class="review-card-rating">
			<?php for ($i = 0; $i<5; $i++):?>
			<?php if ($i<$rating):?>
			<div class="reviews-card-rating-status"></div>
			<?php else: ?>
			<div class="reviews-card-rating-status empty"></div>
			<?php endif; endfor; ?>
		</div>
	</div>
	<div class="review-card-content">
		<div class="review-card-name"><?php echo $comment->comment_author; ?></div>
		<div class="review-card-text">
			<?php echo $comment->comment_content; ?>
		</div>
	</div>

<?php 
}

function themeCommentsPaginationGetArray($array){
	$link = array();
	$i=0;
	foreach ($array as $item){
		$out = array();
		preg_match('/href="(.*?)"/', $item, $out);
		$link[$i]['text'] = strip_tags($item);
		if (strpos($item, 'current')){
			$link[$i]['pos'] = 'current';
		}
		if (strpos($item, 'next')){
			$link[$i]['pos'] = 'next';
		}
		if (strpos($item, 'prev')){
			$link[$i]['pos'] = 'prev';
		}
		$link[$i]['val'] = $out;
		$i++;
	}
	return $link;
}
const {src, dest, parallel, series, watch} = require('gulp');
const browserSync = require('browser-sync').create();
const w3cjs = require('gulp-w3cjs');
const htmlImport = require('gulp-html-imports');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleancss = require('gulp-clean-css');

function browsersync() {
    browserSync.init({
        server: {
            baseDir: 'assets/'
        },
        notify: false,
        online: true
    });
}

function styles(){
    return src([
        'app/**/*.sass',
        'app/**/*.css'
    ])
    .pipe(eval('sass')())
    .pipe(concat('app.min.css'))
    .pipe(autoprefixer({overrideBrowserslist: ['last 10 versions'], grid: true}))
    .pipe(cleancss({level:{1:{specialComments:0}}}))
    .pipe(dest('assets/css/'))
    .pipe(browserSync.stream())
}

function scripts(){
    return src([
        'app/js/*'
    ])
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(dest('assets/js/'))
    .pipe(browserSync.stream())
}

function html(){
    return src('app/*.html')
    .pipe(htmlImport('app/components/'))
    .pipe(dest('assets/'))
    .pipe(browserSync.stream())
}

function fonts(){
    return src([
        'app/fonts/*.ttf',
        'app/fonts/*.woff',
        'app/fonts/*.woff2'
    ])
    .pipe(dest('assets/fonts/'))
    .pipe(browserSync.stream())
}

// function validator(){
//     return src('assets/*.html')
//     .pipe(w3cjs())
//     .pipe(w3cjs.reporter())
// }
function startwatch(){
    watch('app/**/*.js', scripts);
    watch('app/**/*.sass', styles);
    watch('app/**/*.css', styles);
    watch('app/**/*.html', html);;
}


exports.browsersync = browsersync;
exports.scripts = scripts;
exports.styles = styles;
exports.html = html;
// exports.validator = validator;
exports.fonts = fonts;

exports.default = parallel(styles, scripts, html, fonts, browsersync, startwatch);
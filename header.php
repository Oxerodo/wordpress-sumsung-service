<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!-- import "header.html" -->

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>

<body class="body">
    <header>
        <div class="header-top-wrapper">
            <nav class="navbar">
                <div class="container">
                    <div class="row">
                        <div class="wrapper d-flex">
                            <div class="header-top-city-wrapper">
                                <span class="header-top-city">г. Москва</span>
                                <div class="js-header-top-city-list header-top-city-list">
                                    <ul>
                                        <li class data-link="https://samsung-servicecenter.ru/">Москва</li>
                                        <li data-link="https://spb.samsung-servicecenter.ru/">Санкт-Петербург</li>
                                        <li data-link="https://ekaterinburg.samsung-servicecenter.ru/">Екатеринбург</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="navbar-topmenu d-flex justify-content-center sm-d-none">
                                <?php viewHeadMenu([
                                    'theme_location' => 'head_menu',
                                    'container' => false,
                                    'menu_class' => 'navbar-nav',
                                    'menu_item_class' => 'nav-item',
                                    'menu_item_link_class' => 'nav-link'
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <div class="header-middle-wrapper">
            <div class="container">
                <div class="row ">
                    <div class="wrapper d-flex align-items-center">
                        <div class="logotype-wrapper">
                            <div class="logotype-inner">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/logotype.png" alt="" class="logotype-image">
                            </div>
                            <span class="logotype-slogan">Официальный сервисный центр</span>
                        </div>
                        <div class="search-and-phones-wrapper d-flex d-flex align-items-center md-flex-collumn md-align-items-start sm-d-none">
                            <div class="search-wrapper">
                                <?/*
                                <form action="#" class="input-group mb-3">
                                    <input type="search" class="form-control input-search" placeholder="Поиск">
                                    <button class="btn btn-search-submit" type="submit"></button>
                                </form>
                                */?>
                                <?php get_search_form(); ?>
                            </div>
                            <div class="phone-wrapper">
                                <div class="phone-inner">
                                    <a href="tel:+79998887766" class="phone-number">
                                        <span class="phone-number-text">7 (999) 888-77-66</span>
                                    </a>
                                    <div class="phone-number-info">
                                        <span>Ежедневно с 08:00 до 22:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="callback-wrapper sm-d-none">
                            <button class="btn btn-order-call js-openModalFormApllication">Заказать звонок</button>
                            <button class="btn btn-ckeck-status js-openModalFormChecked">Проверить статус</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom-wrapper">
            <div class="container">
                <div class="row">
                    <div class="wrapper">
                        <nav class="mainmenu">
                            <?php viewHeadMenu([
                                'theme_location' => 'nav_menu',
                                'container' => false,
                                'menu_class' => 'mainmenu-nav d-flex justify-content-center',
                                'menu_item_class' => 'mainmenu-item',
                                'menu_item_link_class' => 'mainmenu-link'
                            ]); ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part('template-parts/tpl/banner', 'single'); ?>
    <?php get_template_part('template-parts/tpl/property'); ?>
    <?php get_template_part('template-parts/tpl/pricelist'); ?>
    <?php get_template_part('template-parts/tpl/steps'); ?>
    <?php get_template_part('template-parts/content/content', 'single');?>
    <?php get_template_part('template-parts/tpl/map'); ?>
    <?php get_template_part('template-parts/tpl/slider', 'model'); ?>
<?php endwhile; ?>
<?php get_footer();

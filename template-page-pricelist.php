<?php
/*
Template Name: Прайслист
*/
?>
<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('template-parts/tpl/banner'); ?>
	<?php get_template_part('template-parts/tpl/pricelist', 'all'); ?>
	<?php get_template_part('template-parts/tpl/steps'); ?>
	<?php if (comments_open() || get_comments_number()) : ?>
		<?php get_template_part('template-parts/content/comments'); ?>
	<?php endif; ?>
	<?php get_template_part('template-parts/tpl/map'); ?>
<?php endwhile; ?>
<?php get_footer();
